import datetime
import os

import requests
from celery import Celery
from celery.utils.log import get_task_logger
from pymongo import MongoClient

import utils


API_KEY = os.environ.get('API_KEY')
if not API_KEY:
    raise SystemExit('API_KEY env var not found')
ADVERTS_LIST_URL = 'https://partners-api.999.md/adverts'
ADVERT_DETAILS_URL = 'https://partners-api.999.md/adverts/{advert_id}'

SYNC_INTERVAL_SECONDS = 1 * 60


app = Celery(broker='redis://redis')
logger = get_task_logger(__name__)


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        SYNC_INTERVAL_SECONDS,
        sync_adverts.s(),
        name='sync adverts')


@app.task
def sync_adverts():
    currency_rates = utils.get_national_bank_rates(date=datetime.date.today())
    client = MongoClient('mongodb', 27017, replicaset='rs0')
    db = client['adverts_db']
    page = 1
    advert_ids = []
    while True:
        logger.info(f'Getting page {page}')
        adverts_list_response = requests.get(
            ADVERTS_LIST_URL,
            params={'page': page},
            auth=(API_KEY, '')
        )
        adverts_list_response.raise_for_status()
        adverts_list_page = adverts_list_response.json()['adverts']
        if not adverts_list_page:
            break
        for advert in adverts_list_page:
            logger.info(f'Getting details of advert {advert["id"]}')
            advert_details_response = requests.get(
                ADVERT_DETAILS_URL.format(advert_id=advert['id']),
                auth=(API_KEY, '')
            )
            advert_details_response.raise_for_status()
            advert_data = advert_details_response.json()
            price = advert_data.get('price')
            if price and price['unit'] == 'eur':
                price_in_mdl = price['value'] * currency_rates['EUR']
                price['value'] = price_in_mdl
                price['unit'] = 'mdl'
            advert_ids.append(advert_data['id'])
            db.adverts.replace_one(
                {'_id': advert_data['id']},
                {
                    '_id': advert_data['id'],
                    **advert_data
                },
                upsert=True
            )
        page += 1
    db.adverts.delete_many({'_id': {'$nin': advert_ids}})
