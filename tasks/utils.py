from xml.etree import ElementTree

import requests


NATIONAL_BANK_API_URL = 'https://www.bnm.md/en/official_exchange_rates'


def get_national_bank_rates(date):
    r = requests.get(NATIONAL_BANK_API_URL, params={
        'get_xml': 1,
        'date': date.strftime('%d.%m.%Y')
    })
    tree = ElementTree.fromstring(r.content)
    return {
        currency.find('CharCode').text: float(currency.find('Value').text)
        for currency in tree
    }
