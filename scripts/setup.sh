MONGODB='mongodb'

echo "Waiting for startup.."
until curl http://${MONGODB}:27017/serverStatus\?text\=1 2>&1 | grep uptime | head -1; do
  printf '.'
  sleep 1
done

mongo --host ${MONGODB}:27017 <<EOF
rs.initiate();
rs.status();
EOF