from aiohttp import web
from aiohttp_apispec import request_schema

from schemas import Pagination


@request_schema(Pagination, locations=['query'])
async def get_adverts_list(request):
    limit = request['data']['per_page']
    offset = (request['data']['page'] - 1) * limit
    adverts = request.app['mongo'].adverts.find().skip(offset).limit(limit)
    adverts_list = [advert async for advert in adverts]
    return web.json_response(adverts_list)
