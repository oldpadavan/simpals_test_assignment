from marshmallow import Schema, fields, validate


class Pagination(Schema):
    page = fields.Integer(
        required=False,
        validate=validate.Range(min=1),
        missing=1)
    per_page = fields.Integer(
        required=False,
        validate=validate.Range(min=1),
        missing=10)
