from aiohttp import web
from aiohttp_apispec import setup_aiohttp_apispec, validation_middleware

from settings import Settings
from views import get_adverts_list
import mongo


def make_app():
    app = web.Application()
    app['settings'] = Settings()
    app.router.add_view('/adverts', get_adverts_list)
    setup_aiohttp_apispec(app)
    app.middlewares.append(validation_middleware)
    mongo.setup(app)
    return app


def run():
    app = make_app()
    web.run_app(app)


if __name__ == '__main__':
    run()
