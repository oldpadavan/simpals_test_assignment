from pydantic import BaseSettings


class Settings(BaseSettings):
    db_name = 'adverts_db'
    mongo_uri = 'mongodb://mongodb/?replicaSet=rs0'
